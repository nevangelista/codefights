#include <algorithm>
#include <iostream>
#include <list>
#include <map>
#include <set>
#include <string>
#include <vector>

std::map<std::pair<std::string, std::string>, int> distmap;

int editDistanceR(std::string s, std::string t) {
    if (s.empty()) {
        return t.length();
    }

    if (t.empty()) {
        return s.length();
    }

    auto key = std::make_pair(s, t);
    auto inset = distmap.find(key);
    if (inset != distmap.end()) {
        return inset->second;
    }

    auto cost = 0;
    if (s[s.length() - 1] != t[t.length() - 1]) {
        cost = 1;
    }

    auto levdist = std::min(
            editDistanceR(s.substr(0, s.length() - 1), t) + 1,
            std::min(
                    editDistanceR(s, t.substr(0, t.length() - 1)) + 1,
                    editDistanceR(s.substr(0, s.length() - 1), t.substr(0, t.length() - 1))
                            + cost));

    return levdist;
}

int editDistance(std::string s, std::string t) {
    if (s.empty()) {
        return t.length();
    }

    if (t.empty()) {
        return s.length();
    }

    auto m = s.length();
    auto n = t.length();

    int distances[m + 1][n + 1];
    for (auto i = 0; i <= m; ++i) {
        for (auto j = 0; j <= n; ++j) {
            if (i == 0) {
                distances[i][j] = j;
            } else if (j == 0) {
                distances[i][j] = i;
            } else if (s[i - 1] == t[j - 1]) {
                distances[i][j] = distances[i - 1][j - 1];
            } else {
                distances[i][j] = 1
                        + std::min(distances[i][j - 1],
                                   std::min(distances[i - 1][j], distances[i - 1][j - 1]));
            }
        }
    }

    return distances[m][n];
}

bool rearrange(std::list<std::string>& others, const std::string& current, int index) {
    std::cout << current << "@" << index << " vs ";
    for (auto other : others) {
        std::cout << other << " ";
    }
    std::cout << std::endl;

    for (auto otra = others.begin(); otra != others.end(); ++otra) {
        auto dist = editDistance(current, *otra);
        if ((dist == 1) && (index != std::distance(others.begin(), otra) - 1)) {
            if (otra != others.begin()) {
                auto prev = std::prev(otra);
                if (editDistance(current, *prev) == 1) {
                    others.insert(otra, current);
                    std::cout << "Returning true @ " << *otra << std::endl;
                    return true;
                }
            } else {
                others.insert(otra, current);
                std::cout << "Returning true @ " << *otra << std::endl;
                return true;
            }
        }
    }

    return false;
}

void eraseCandidate(
        std::list<std::string>& arranged,
        const std::string& candidate,
        std::size_t pos) {
    auto nuke = arranged.begin();
    while (nuke != arranged.end()) {
        nuke = std::find(nuke, arranged.end(), candidate);
        if (nuke != arranged.end()) {
            auto where = std::distance(arranged.begin(), nuke);
            std::cout << "Found " << candidate << "@" << where << std::endl;
            if (pos == where) {
                arranged.erase(nuke);
                return;
            } else {
                ++nuke;
            }
        }
    }
}

bool stringsRearrangement(std::vector<std::string> inputArray) {
    std::set<int> farset;
    bool ok = true;
    for (auto i = 0; i < inputArray.size() - 1; ++i) {
        auto dist = editDistance(inputArray[i], inputArray[i + 1]);
        ok &= (dist == 1);
        if (dist != 1) {
            farset.insert(i + 1);
        }
    }

    if (ok) {
        return ok;
    } else {
        ok = true;
        std::list<std::string> arranged(inputArray.begin(), inputArray.end());
        auto origsize = arranged.size();
        auto cursize = arranged.size();
        for (auto inset : farset) {
            auto offset = cursize - origsize;
            eraseCandidate(arranged, inputArray[inset], inset + offset);
            ok &= rearrange(arranged, inputArray[inset], inset);
            cursize = arranged.size();
        }
    }

    return ok;
}

int main() {
    std::vector<std::string> inputArray{
            //"abc", "xbc", "xxc", "xbc", "aby", "ayy", "aby", "azc"
            "abbabbba",
            "abbabbca",
            "abbasbba",
            "abbsbbba",
            "acbabbba"
            //"abc", "abx", "axx", "abc"
    };

    std::cout << std::boolalpha << stringsRearrangement(inputArray) << std::endl;
}
