#include <iostream>
#include <set>

int extraNumber(int a, int b, int c) {
    std::set<int> numset;
    auto [aiter, ares]{numset.insert(a)};
    auto [biter, bres]{numset.insert(b)};
    if (!bres) {
        return c;
    }

    auto [citer, cres]{numset.insert(c)};
    if (!cres) {
        if (*biter == *citer) {
            return a;
        } else {
            return b;
        }
    }

    return 0;
}

int main() {
    std::cout << extraNumber(3, 2, 2) << std::endl;
    std::cout << extraNumber(5, 5, 1) << std::endl;
}