#include <cmath>
#include <iostream>

int depositProfit(int deposit, int rate, int threshold) {
    auto accrued = 0.0;
    auto year = 0;
    while (accrued < threshold) {
        ++year;
        accrued = deposit * std::pow(1.0 + 0.01 * rate, year);
    }

    return year;
}

int main() {
    std::cout << depositProfit(100, 20, 170);
    return 0;
}