//
// Binary trees are already defined with this interface:
// template<typename T>
// struct Tree {
//   Tree(const T &v) : value(v), left(nullptr), right(nullptr) {}
//   T value;
//   Tree *left;
//   Tree *right;
// };
#include <stack>

template<typename T>
struct Tree {
    T value;
    Tree* left{nullptr};
    Tree* right{nullptr};
};

int kthSmallestInBST(Tree<int>* t, int k) {
    std::size_t nth{0u};
    std::stack<Tree<int>*> stack;
    auto node{t};
    while (!stack.empty() || node != nullptr) {
        if (node != nullptr) {
            stack.push(node);
            node = node->left;
        } else {
            node = stack.top();
            stack.pop();
            if (++nth == k) {
                return node->value;
            }
            node = node->right;
        }
    }

    return 0;
}

int main() {}
