#include <iostream>
#include <set>
#include <string>

int commonCharacterCount(std::string s1, std::string s2) {
    std::multiset<char> ins1;
    for (auto c1 : s1) {
        ins1.insert(c1);
    }

    std::multiset<char> ins2;
    for (auto c2 : s2) {
        ins2.insert(c2);
    }

    int num_common = 0;
    std::set<char> found;
    for (auto in1 : ins1) {
        auto in2 = ins2.find(in1);
        if (in2 != ins2.end()) {
            if (found.find(in1) == found.end()) {
                found.insert(in1);
                num_common += std::min(ins1.count(in1), ins2.count(in1));
            }
        }
    }

    return num_common;
}

int main() {
    std::string s1{"aabcc"};
    std::string s2{"adcaa"};
    auto num_common = commonCharacterCount(s1, s2);
    std::cout << num_common << std::endl;
}
