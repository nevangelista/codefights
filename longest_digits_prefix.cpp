#include <iostream>
#include <string>

std::string longestDigitsPrefix(std::string inputString) {
    std::string outString;
    for (auto inchar : inputString) {
        if (std::isdigit(inchar)) {
            outString.append(1, inchar);
        } else {
            return outString;
        }
    }

    return outString;
}

int main() {
    std::string inputString{"123aa1"};

    std::cout << longestDigitsPrefix(inputString) << std::endl;
}
