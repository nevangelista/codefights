#include <iostream>
#include <queue>

template<typename T>
struct Tree {
    T value;
    Tree* left{nullptr};
    Tree* right{nullptr};
};

Tree<int>* find(Tree<int>* t, int value) {
    if (t == nullptr) {
        return nullptr;
    }

    if (t->value == value) {
        return t;
    }

    std::queue<Tree<int>*> tq;
    if (t->left != nullptr) {
        tq.push(t->left);
    }

    if (t->right != nullptr) {
        tq.push(t->right);
    }

    while (!tq.empty()) {
        auto node{tq.front()};
        tq.pop();

        if (node->value == value) {
            return node;
        }

        if (node->left != nullptr) {
            tq.push(node->left);
        }

        if (node->right != nullptr) {
            tq.push(node->right);
        }
    }

    return nullptr;
}

bool equal(Tree<int>* t1, Tree<int>* t2) {
    if (t1 == nullptr && t2 == nullptr) {
        return true;
    }

    if (t1 == nullptr || t2 == nullptr) {
        return false;
    }

    return equal(t1->left, t2->left) && equal(t1->right, t2->right);
}

bool isSubtree(Tree<int>* t1, Tree<int>* t2) {
    if (t1 == nullptr || t2 == nullptr) {
        return false;
    }

    Tree<int>* subtree{find(t1, t2->value)};
    if (subtree != nullptr) {
        std::cout << "Found t2 root value " << subtree->value << std::endl;
        return equal(subtree, t2);
    }

    return false;
}

int main() {}