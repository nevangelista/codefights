#include <iostream>
#include <string>

char firstDigit(std::string inputString) {
    for (auto input : inputString) {
        if (std::isdigit(input)) {
            return input;
        }
    }

    return ' ';
}

int main() {
    std::string inputString{"var_1__Int"};
    std::cout << firstDigit(inputString) << std::endl;
}