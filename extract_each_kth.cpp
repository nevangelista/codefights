#include <algorithm>
#include <iostream>
#include <vector>

std::vector<int> extractEachKth(std::vector<int> inputArray, int k) {
    std::vector<int> outArray;
    for (auto i = 0; i < inputArray.size(); ++i) {
        if (0 == (i + 1) % k) {
            continue;
        } else {
            outArray.push_back(inputArray[i]);
        }
    }

    return outArray;
}

int main() {
    std::vector<int> inputArray{1, 2, 3, 4, 5, 6, 7, 8, 9, 10};

    std::vector<int> outArray = extractEachKth(inputArray, 3);
    for (auto out : outArray) {
        std::cout << out << " ";
    }
    std::cout << std::endl;
}
