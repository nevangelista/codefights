#include <iostream>
#include <limits>
#include <numeric>
#include <vector>

int arrayMaxConsecutiveSum(std::vector<int> inputArray, int k) {
    auto cur_sum = std::accumulate(inputArray.begin(), inputArray.begin() + k, 0);
    auto max_sum = cur_sum;

    for (auto i = 1; i <= inputArray.size() - k; ++i) {
        cur_sum = cur_sum - inputArray[i - 1] + inputArray[i + k - 1];
        max_sum = std::max(max_sum, cur_sum);
    }

    return max_sum;
}

int main() {
    std::vector<int> inputArray{2, 3, 5, 1, 6};

    std::cout << arrayMaxConsecutiveSum(inputArray, 2) << std::endl;
}