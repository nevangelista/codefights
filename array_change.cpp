#include <iostream>
#include <vector>

int arrayChange(std::vector<int> inputArray) {
    auto changes = 0;
    auto last = inputArray[0];
    for (auto i = 1; i < inputArray.size(); ++i) {
        if (inputArray[i] > last) {
            last = inputArray[i];
            continue;
        } else {
            auto increase = last - inputArray[i] + 1;
            changes += increase;
            inputArray[i] += increase;
            last = inputArray[i];
        }
    }

    return changes;
}

int main() {
    std::vector<int> input{-1000, 0, -2, 0};

    std::cout << arrayChange(input) << std::endl;

    return 0;
}
