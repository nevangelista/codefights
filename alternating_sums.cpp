#include <iostream>
#include <numeric>
#include <vector>

std::vector<int> alternatingSums(std::vector<int> a) {
    std::vector<int> team1;
    std::vector<int> team2;
    for (auto i = 0; i < a.size(); ++i) {
        if ((i + 1) % 2 == 0) {
            team2.push_back(a[i]);
        } else {
            team1.push_back(a[i]);
        }
    }

    auto weights1 = std::accumulate(team1.begin(), team1.end(), 0);
    auto weights2 = std::accumulate(team2.begin(), team2.end(), 0);

    return std::vector<int>{weights1, weights2};
}

int main() {
    std::vector<int> peeps{50, 60, 60, 45, 70};

    std::vector<int> weights = alternatingSums(peeps);
    for (auto weight : weights) {
        std::cout << weight << ", ";
    }

    std::cout << std::endl;
    return 0;
}
