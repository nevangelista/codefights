#include <algorithm>
#include <cmath>
#include <iostream>
#include <limits>
#include <vector>

//! \fn
int avoidObstacles(std::vector<int> inputArray) {
    std::sort(inputArray.begin(), inputArray.end());
    auto minit = std::min_element(inputArray.begin(), inputArray.end());
    auto maxit = std::max_element(inputArray.begin(), inputArray.end());

    std::vector<int> filled;
    for (auto i = 2; i < *maxit + 1; ++i) {
        filled.push_back(i);
    }

    std::vector<int> gaps;
    for (auto fill : filled) {
        if (std::find(inputArray.begin(), inputArray.end(), fill) == inputArray.end()) {
            gaps.push_back(fill);
        }
    }

    if (gaps.empty()) {
        return *maxit + 1;
    } else {
        for (auto gap : gaps) {
            bool lands = false;
            for (auto input : inputArray) {
                if (input % gap == 0) {
                    lands = true;
                    break;
                }
            }
            if (!lands) {
                return gap;
            }
        }
    }

    return 0;
}

int main() {
    std::vector<int> inputArray{
            19, 32, 11, 23
            // 2, 3
    };

    auto gap = avoidObstacles(inputArray);
    std::cout << gap << std::endl;

    return 0;
}
