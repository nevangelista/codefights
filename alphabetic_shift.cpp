#include <iostream>
#include <string>

std::string alphabeticShift(std::string inputString) {
    for (auto& inst : inputString) {
        if (inst == 'z') {
            inst = 'a';
        } else {
            ++inst;
        }
    }

    return inputString;
}

int main() {
    std::cout << alphabeticShift("crazy") << std::endl;

    return 0;
}