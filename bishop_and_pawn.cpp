#include <iostream>
#include <map>
#include <string>

bool bishopAndPawn(std::string bishop, std::string pawn) {
    std::map<char, int> colmap{
            {'a', 1}, {'b', 2}, {'c', 3}, {'d', 4}, {'e', 5}, {'f', 6}, {'g', 7}, {'h', 8}};

    auto bcol = colmap[bishop[0]];
    auto pcol = colmap[pawn[0]];

    auto brow = bishop[1] - '0';
    auto prow = pawn[1] - '0';

    return (std::abs(bcol - pcol) == std::abs(brow - prow));
}

int main() {
    std::cout << std::boolalpha << bishopAndPawn("a1", "c3") << std::endl;
    ;
    std::cout << std::boolalpha << bishopAndPawn("h1", "h3") << std::endl;
    ;
}
