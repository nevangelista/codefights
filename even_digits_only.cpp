#include <iostream>
#include <string>

bool evenDigitsOnly(int n) {
    std::string digits = std::to_string(n);
    bool evens = true;
    for (auto dig : digits) {
        auto val = dig - '0';
        evens &= !(dig & 1);
    }

    return evens;
}

int main() {
    std::cout << std::boolalpha << evenDigitsOnly(248622) << std::endl;
    std::cout << std::boolalpha << evenDigitsOnly(642386) << std::endl;

    return 0;
}
