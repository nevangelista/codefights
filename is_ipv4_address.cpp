#include <cctype>
#include <iostream>
#include <sstream>
#include <string>

bool verifyChunk(std::string& chunk) {
    if (chunk.empty()) {
        return false;
    }

    for (auto digit : chunk) {
        if (!std::isdigit(digit)) {
            return false;
        }
    }

    constexpr int lowest = 0;
    constexpr int highest = 255;
    auto value = std::stoi(chunk, nullptr, 10);
    return (static_cast<unsigned>(value - lowest) <= (highest - lowest));
}

bool isIPv4Address(std::string inputString) {
    std::stringstream ss;
    ss.str(inputString);
    std::string chunk;
    bool is_valid = true;
    auto pieces = 0;
    while (std::getline(ss, chunk, '.')) {
        is_valid &= verifyChunk(chunk);
        ++pieces;
    }

    return is_valid && (pieces == 4);
}

int main() {
    std::string ipv4{".254.255.0"};

    std::cout << std::boolalpha << isIPv4Address(ipv4) << std::endl;

    return 0;
}
