#include <algorithm>
#include <iostream>
#include <vector>

int electionsWinners(std::vector<int> votes, int k) {
    auto numPossible{0};
    auto mostVotes{0};
    auto leader{std::max_element(votes.begin(), votes.end())};

    for (auto vote : votes) {
        if (vote == *leader) {
            ++mostVotes;
        } else if (vote + k > *leader) {
            ++numPossible;
        }
    }

    if (k > 0) {
        numPossible += mostVotes;
    } else if (mostVotes == 1) {
        ++numPossible;
    }

    return numPossible;
}

int main() {
    std::vector<int> test1{2, 3, 5, 2};
    std::cout << std::boolalpha << (electionsWinners(test1, 3) == 2) << std::endl;
}