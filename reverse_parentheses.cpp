#include <algorithm>
#include <iostream>
#include <string>

bool isBalanced(std::string& s) {
    auto lp = s.find_first_of('(');
    if (lp != std::string::npos) {
        auto rp = s.find_last_of(')');
        return (lp < rp);
    } else {
        return true;
    }
}

std::string reverseParentheses(std::string s) {
    auto lp = s.find_first_of('(');
    if (lp != std::string::npos) {
        auto rp = s.find_last_of(')');
        std::string mid = s.substr(lp + 1, rp - lp - 1);
        if (isBalanced(mid)) {
            std::string lh = s.substr(0, lp);
            std::string rh = s.substr(rp + 1);
            std::string revved = reverseParentheses(mid);
            std::reverse(revved.begin(), revved.end());
            return (lh + revved + rh);
        } else {
            auto rp = s.find_first_of(')');
            std::string lh = s.substr(0, rp + 1);
            std::string rh = s.substr(rp + 1);
            std::string revved = reverseParentheses(lh) + reverseParentheses(rh);
            return revved;
        }
    } else {
        return s;
    }
}

int main() {
    std::string do180{"a(bc)de"};
    std::string revved = reverseParentheses(do180);
    std::cout << revved << std::endl;

    std::string unbal{"abc(cba)ab(bac)c"};
    revved = reverseParentheses(unbal);
    std::cout << revved << std::endl;
    return 0;
}