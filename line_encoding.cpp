#include <iostream>
#include <set>
#include <sstream>
#include <string>
#include <typeinfo>

std::string lineEncoding(std::string s) {
    auto count{0};
    char current{s[0]};
    int currentCount{0};

    std::stringstream ss;
    for (auto achar : s) {
        if (achar != current) {
            if (currentCount > 1) {
                ss << currentCount;
                currentCount = 1;
            }

            ss << current;
            current = achar;
        } else {
            ++currentCount;
        }

        if (count == (s.length() - 1)) {
            if (currentCount > 1) {
                ss << currentCount;
            }

            ss << current;
        }
        ++count;
    }

    return ss.str();
}

int main() {
    std::string test1{"aabbbc"};
    std::cout << std::boolalpha << (lineEncoding(test1) == std::string("2a3bc")) << std::endl;
    std::string test2{"abbcabb"};
    std::cout << std::boolalpha << (lineEncoding(test2) == std::string("a2bca2b")) << std::endl;
}