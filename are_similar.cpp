#include <algorithm>
#include <cmath>
#include <iostream>
#include <vector>

bool areEqualSorted(std::vector<int> a, std::vector<int> b) {
    std::sort(a.begin(), a.end());
    std::sort(b.begin(), b.end());
    return std::equal(a.begin(), a.end(), b.begin());
}

bool areSimilar(std::vector<int> a, std::vector<int> b) {
    auto where = std::mismatch(a.begin(), a.end(), b.begin());
    if (where.first == a.end() && where.second == b.end()) {
        return true;
    } else {
        auto num_mismatches = 0;
        while (where.first != a.end() || where.second != b.end()) {
            ++num_mismatches;
            where = std::mismatch(++where.first, a.end(), ++where.second);
        }

        return ((num_mismatches == 2) && areEqualSorted(a, b));
    }
}

int main() {
    std::vector<int> a{1, 2, 3, 4, 5, 7, 6, 9, 11};

    std::vector<int> b{1, 2, 3, 5, 4, 7, 6, 8, 11};

    std::cout << std::boolalpha << areSimilar(a, b) << std::endl;

    return 0;
}
