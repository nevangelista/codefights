#include <algorithm>
#include <iostream>
#include <iterator>
#include <vector>

std::vector<int> sortByHeight(std::vector<int> a) {
    std::vector<int> trees;
    std::vector<int> folks;
    auto lastit = a.begin();
    for (auto here = a.begin(); here != a.end(); ++here) {
        if (*here == -1) {
            trees.push_back(std::distance(a.begin(), here));
            lastit = here;
        } else {
            folks.push_back(*here);
        }
    }

    std::sort(folks.begin(), folks.end());
    if (trees.empty()) {
        return folks;
    }

    auto folk = std::begin(folks);
    auto tree = std::begin(trees);
    std::vector<int> sorted;
    for (auto i = 0; i < a.size(); ++i) {
        if (*tree == i) {
            sorted.push_back(-1);
            ++tree;
        } else {
            sorted.push_back(*folk);
            ++folk;
        }
    }

    return sorted;
}

int main() {
    std::vector<int> park{4, 2, 9, 11, 2, 16};

    std::vector<int> sorted = sortByHeight(park);
    return 0;
}
