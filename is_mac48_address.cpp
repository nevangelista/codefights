#include <cctype>
#include <iostream>
#include <sstream>
#include <string>
#include <vector>

bool isMAC48Address(std::string inputString) {
    auto numDashes{0};
    auto dash{inputString.find_first_of('-')};
    bool isMac48{true};
    auto pez{inputString.substr(0, dash)};
    while (dash != std::string::npos) {
        ++numDashes;
        pez = inputString.substr(0, dash);
        bool isPezHex{(std::isxdigit(pez[0]) && std::isxdigit(pez[1]))};
        isMac48 &= isPezHex && (pez.length() == 2);
        inputString = inputString.substr(dash + 1);
        dash = inputString.find_first_of('-');
    }

    bool isPezHex{(std::isxdigit(inputString[0]) && std::isxdigit(inputString[1]))};
    isMac48 &= isPezHex && (inputString.length() == 2);
    return isMac48 && (numDashes == 5);
}

int main() {
    std::string test1{"00-1B-63-84-45-E6"};
    std::cout << std::boolalpha << (isMAC48Address(test1) == true) << std::endl;
    std::string test2{"Z1-1B-63-84-45-E6"};
    std::cout << std::boolalpha << (isMAC48Address(test2) == false) << std::endl;
    std::string test3{"02-03-04-05-06-07-"};
    std::cout << std::boolalpha << (isMAC48Address(test3) == false) << std::endl;
    std::string test4{"12-34-56-78-9A-BG"};
    std::cout << std::boolalpha << (isMAC48Address(test4) == false) << std::endl;
}
