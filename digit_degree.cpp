#include <iostream>
#include <string>

int digitDegree(int n) {
    auto ntimes = 0;
    while (n / 10) {
        auto nun = 0;
        while (n > 0) {
            nun += n % 10;
            n /= 10;
        }
        n = nun;
        ++ntimes;
    }

    return ntimes;
}

int main() {
    std::cout << digitDegree(5) << std::endl;
    std::cout << digitDegree(100) << std::endl;
    std::cout << digitDegree(91) << std::endl;
}
