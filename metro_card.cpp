#include <iostream>
#include <map>
#include <set>
#include <vector>

namespace {
const std::multimap<int, int> gMonDays{
        {31, 28},
        {28, 31},
        {31, 30},
        {30, 31},
        {31, 30},
        {30, 31},
        {31, 31},
        {31, 30},
        {30, 31},
        {31, 30},
        {30, 31},
        {31, 31}};
}  // namespace

std::vector<int> metroCard(int lastNumberOfDays) {
    std::set<int> foundlings;
    auto inDays{gMonDays.equal_range(lastNumberOfDays)};

    for (auto val = inDays.first; val != inDays.second; ++val) {
        foundlings.insert(val->second);
    }

    std::cout << "Returning " << foundlings.size() << " values" << std::endl;

    return {foundlings.begin(), foundlings.end()};
}

void printVector(std::vector<int>& recharge) {
    std::cout << "[";
    for (auto idx = 0; idx < recharge.size() - 1; ++idx) {
        std::cout << recharge[idx] << ", ";
    }

    std::cout << recharge[recharge.size() - 1];

    std::cout << "]" << std::endl;
}

int main() {
    std::vector<int> recharge{metroCard(30)};
    printVector(recharge);

    recharge = metroCard(31);
    printVector(recharge);
}