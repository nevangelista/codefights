#include <iostream>

bool isInfiniteProcess(int a, int b) {
    auto idx{0};
    while (a != b) {
        ++a;
        --b;

        if (++idx > 40) {
            return true;
        }
    }

    return false;
}

int main() {
    std::cout << std::boolalpha << (isInfiniteProcess(2, 6) == false) << std::endl;
    std::cout << std::boolalpha << (isInfiniteProcess(2, 3) == true) << std::endl;
    std::cout << std::boolalpha << (isInfiniteProcess(2, 10) == false) << std::endl;
    std::cout << std::boolalpha << (isInfiniteProcess(0, 1) == true) << std::endl;
    std::cout << std::boolalpha << (isInfiniteProcess(3, 1) == true) << std::endl;
    std::cout << std::boolalpha << (isInfiniteProcess(10, 10) == false) << std::endl;
    std::cout << std::boolalpha << (isInfiniteProcess(5, 10) == true) << std::endl;
    std::cout << std::boolalpha << (isInfiniteProcess(6, 10) == false) << std::endl;
    std::cout << std::boolalpha << (isInfiniteProcess(10, 0) == true) << std::endl;
}