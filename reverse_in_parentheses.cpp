#include <algorithm>
#include <iostream>
#include <stack>
#include <string>

std::string reverseInParentheses(std::string inputString) {
    constexpr char LPAREN{'('};
    constexpr char RPAREN{')'};
    std::stack<std::string> daStack;

    std::string ouster;
    for (auto ess : inputString) {
        if (ess == LPAREN) {
            daStack.push(ouster);
            ouster.clear();
        } else if (ess == RPAREN) {
            std::string instr(ouster);
            std::reverse(instr.begin(), instr.end());
            ouster = daStack.top();
            ouster.append(instr);
            daStack.pop();
        } else {
            ouster.append({ess});
        }
    }

    return ouster;
}

int main() {
    std::string test1{"(bar)"};
    std::cout << reverseInParentheses(test1) << std::endl;

    std::string test2{"foo(bar)baz"};
    std::cout << reverseInParentheses(test2) << std::endl;

    std::string test3("foo(bar)baz(blim)");
    std::cout << reverseInParentheses(test3) << std::endl;

    std::string test4("foo(bar(baz))blim");
    std::cout << reverseInParentheses(test4) << std::endl;

    std::string test5("");
    std::cout << reverseInParentheses(test5) << std::endl;

    std::string test6("()");
    std::cout << reverseInParentheses(test6) << std::endl;

    std::string test7("(abc)d(efg)");
    std::cout << reverseInParentheses(test7) << std::endl;

    std::string test9{"(())(((())))"};
    std::cout << reverseInParentheses(test9) << std::endl;
}