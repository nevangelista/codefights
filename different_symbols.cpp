#include <iostream>
#include <set>

int differentSymbolsNaive(std::string s) {
    std::set<char> uneeq;
    for (auto ins : s) {
        uneeq.insert(ins);
    }

    return uneeq.size();
}

int main() {
    std::string test{"cabca"};

    std::cout << differentSymbolsNaive(test) << std::endl;
}