#include <iostream>
#include <string>
#include <vector>

std::vector<std::string> addBorder(std::vector<std::string> picture) {
    auto len = picture[0].length();
    len += 2;

    std::string frame(len, '*');
    std::vector<std::string> framed{frame};

    for (auto pic : picture) {
        std::string framer = "*" + pic + "*";
        framed.push_back(framer);
    }

    framed.push_back(frame);

    return framed;
}

int main() {
    std::vector<std::string> picture{"abc", "ded"};
}
