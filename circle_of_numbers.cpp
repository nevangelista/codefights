#include <iostream>

int circleOfNumbers(int n, int firstNumber) {
    auto halfling = n / 2;
    auto radial = (firstNumber < halfling) ? firstNumber + halfling : firstNumber - halfling;
    return radial;
}

int main() {
    std::cout << circleOfNumbers(10, 2) << std::endl;
}