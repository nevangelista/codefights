#include <algorithm>
#include <iostream>
#include <string>
#include <vector>


std::vector<std::string> allLongestStrings(std::vector<std::string> inputArray) {

    auto lenless = [](const std::string& lh, const std::string& rh) {
        return lh.length() < rh.length();
    };
    auto longest = std::max_element(inputArray.begin(), inputArray.end(), lenless);
    auto reflen = longest->size();
    std::vector<std::string> all_longest;
    all_longest.push_back(*longest);
    inputArray.erase(longest);
    while (longest != inputArray.end()) {
        longest = std::max_element(inputArray.begin(), inputArray.end(), lenless);
        if (longest->size() == reflen) {
            all_longest.push_back(*longest);
            inputArray.erase(longest);
        } else {
            break;
        }
    }

    return all_longest;
}

int main() {
    std::vector<std::string> input{"aba", "aa", "ad", "vcd", "aba"};

    std::vector<std::string> all_longest = allLongestStrings(input);
    for (auto str : all_longest) {
        std::cout << str << " ";
    }
    std::cout << std::endl;

    return 0;
}