#include <algorithm>
#include <iostream>
#include <limits>
#include <string>

bool isPalindrome(std::string st) {
    auto half = ((st.length() % 2) == 0) ? st.length() / 2 : (st.length() / 2 + 1);
    std::string rh{st.substr(half, st.length())};
    std::string lh{st.substr(0, st.length() / 2)};

    std::reverse(rh.begin(), rh.end());
    return (lh == rh);
}

std::string buildPalindrome(std::string& st) {
    std::string best;
    auto shortest = std::numeric_limits<int>::max();

    for (auto pit = st.rbegin(); pit != st.rend(); ++pit) {
        std::string left{pit + 1, st.rend()};
        std::string nustr{st + left};

        if (isPalindrome(nustr)) {
            if (nustr.length() < shortest) {
                shortest = nustr.length();
                best = nustr;
            }
        }
    }

    return best;
}

int main() {
    std::string st{"abcdc"};
    std::cout << buildPalindrome(st) << std::endl;

    st = "ababab";
    std::cout << buildPalindrome(st) << std::endl;
}
