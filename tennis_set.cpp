#include <iostream>

bool tennisSet(int score1, int score2) {
    if ((score1 == 5 || score2 == 5) && (score1 != score2)) {
        return (score1 == 7 || score2 == 7);
    }

    if ((score1 == 6 || score2 == 6) && (score1 != score2)) {
        if (score1 < 5 || score2 < 5) {
            return true;
        } else if (score1 == 7 || score2 == 7) {
            return true;
        }
    }

    return false;
}

int main() {
    std::cout << std::boolalpha << (tennisSet(3, 6) == true) << std::endl;
    std::cout << std::boolalpha << (tennisSet(8, 5) == false) << std::endl;
    std::cout << std::boolalpha << (tennisSet(6, 5) == false) << std::endl;
    std::cout << std::boolalpha << (tennisSet(7, 6) == true) << std::endl;
}