#include <iostream>
#include <string>

std::string findEmailDomain(std::string address) {
    auto per = address.rfind('@');

    if (per != std::string::npos) {
        return address.substr(per + 1);
    }

    return std::string();
}

int main() {
    std::cout << findEmailDomain("prettyandsimple@example.com") << std::endl;
    std::cout << findEmailDomain("<>[]:,;@\"!#$%&*+-/=?^_{}| ~.a\"@example.org") << std::endl;
}
