#include <algorithm>
#include <iostream>
#include <numeric>
#include <vector>

int matrixElementsSum(std::vector<std::vector<int>> matrix) {

    int total_price = 0;
    for (auto i = 0; i < matrix.size(); ++i) {
        for (auto j = 0; j < matrix[i].size(); ++j) {
            if (i > 0) {
                bool haunted = false;
                for (auto k = i - 1; k >= 0; --k) {
                    if (matrix[k][j] == 0) {
                        haunted |= true;
                    }
                }

                if (haunted) {
                    continue;
                }
            }

            total_price += matrix[i][j];
        }
    }

    return total_price;
}

int main() {
    std::vector<std::vector<int>> matrix{{1, 1, 1, 0}, {0, 5, 0, 1}, {2, 1, 3, 10}};

    std::cout << matrixElementsSum(matrix) << std::endl;

    return 0;
}