#include <iostream>

bool willYou(bool young, bool beautiful, bool loved) {
    bool ybnl{young && beautiful && !loved};
    bool lnynb{loved && (!young || !beautiful)};

    return (ybnl || lnynb);
}

int main() {
    std::cout << std::boolalpha << (willYou(true, true, true) == false) << std::endl;
    std::cout << std::boolalpha << (willYou(true, false, true) == true) << std::endl;
}