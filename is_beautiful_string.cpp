#include <algorithm>
#include <iostream>
#include <set>
#include <string>

bool isBeautifulString(std::string inputString) {
    std::multiset<char> booty;

    for (auto ins : inputString) {
        booty.insert(ins);
    }

    auto prev = booty.begin();
    if (*prev != 'a') {
        return false;
    }

    auto prevcount = booty.count(*prev);
    bool bootiful = true;

    for (auto boo = booty.upper_bound(*prev); boo != booty.end(); boo = booty.upper_bound(*boo)) {
        bootiful &= ((booty.count(*boo) <= prevcount) && ((*boo - *prev) == 1));
        prevcount = booty.count(*boo);
        prev = boo;
    }

    return bootiful;
}

int main() {
    std::cout << std::boolalpha << isBeautifulString("bbbaacdafe") << std::endl;
    std::cout << std::boolalpha << isBeautifulString("bbc") << std::endl;
}
