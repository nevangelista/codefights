#include <cctype>
#include <iostream>

bool variableName(std::string name) {
    if (std::isdigit(name[0])) {
        return false;
    }

    bool ok = true;

    for (auto nom : name) {
        ok &= std::isalnum(nom) || (nom == '_');
    }

    return ok;
}

int main() {
    std::cout << std::boolalpha << variableName("2w2") << std::endl;

    return 0;
}