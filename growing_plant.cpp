#include <iostream>

int growingPlant(int upSpeed, int downSpeed, int desiredHeight) {
    auto numDays = 0;
    auto numNites = 0;
    auto curHeight = 0;

    while (curHeight < desiredHeight) {
        curHeight += upSpeed;
        ++numDays;

        if (curHeight >= desiredHeight) {
            break;
        }

        curHeight -= downSpeed;
        ++numNites;
    }

    return numDays;
}

int main() {
    std::cout << growingPlant(100, 10, 910) << " days" << std::endl;
    std::cout << growingPlant(10, 9, 4) << " days" << std::endl;
    std::cout << growingPlant(5, 2, 7) << " days" << std::endl;
    std::cout << growingPlant(7, 3, 443) << " days" << std::endl;
}