#include <iostream>

bool arithmeticExpression(int a, int b, int c) {
    bool isAdd{a + b == c};
    if (isAdd) {
        return true;
    }

    bool isSub{a - b == c};
    if (isSub) {
        return true;
    }

    bool isMult{a * b == c};
    if (isMult) {
        return true;
    }

    bool isDiv{float(a / float(b)) == float(c)};
    if (isDiv) {
        return true;
    }

    return false;
}

int main() {
    std::cout << std::boolalpha << (arithmeticExpression(2, 3, 5) == true) << std::endl;
    std::cout << std::boolalpha << (arithmeticExpression(8, 2, 4) == true) << std::endl;
    std::cout << std::boolalpha << (arithmeticExpression(8, 3, 2) == false) << std::endl;
}
