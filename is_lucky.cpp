#include <algorithm>
#include <iostream>
#include <string>
#include <vector>

bool isLucky(int n) {
    std::string docky = std::to_string(n);
    auto sum1 = 0;
    for (auto s1 = 0; s1 != docky.length() / 2; ++s1) {
        sum1 += docky[s1] - '0';
    }

    auto sum2 = 0;
    for (auto s2 = docky.length() / 2; s2 < docky.length(); ++s2) {
        sum2 += docky[s2] - '0';
    }

    return (sum1 == sum2);
}

int main() {
    int ducky = 239017;
    std::cout << ducky << " lucky? " << std::boolalpha << isLucky(ducky) << std::endl;
}