#include <iostream>
#include <set>
#include <string>

bool palindromeRearranging(std::string inputString) {
    std::multiset<char> inputs;
    for (auto inchar : inputString) {
        inputs.insert(inchar);
    }

    auto num_odds = 0;
    std::set<char> seen;
    for (auto chars : inputs) {
        if (seen.find(chars) == seen.end()) {
            seen.insert(chars);
        } else {
            continue;
        }

        if ((inputs.count(chars) % 2) != 0) {
            ++num_odds;
        }
    }

    return (num_odds <= 1);
}

int main() {
    std::string inputString{"aaaaaaaaaaaaaaaaaaaaaaaaaaaaaabc"};

    std::cout << std::boolalpha << palindromeRearranging(inputString) << std::endl;

    return 0;
}
