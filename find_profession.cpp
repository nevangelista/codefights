#include <iostream>
#include <string>

std::string findProfession(int level, int pos) {
    if (level == 1) {
        return {"Engineer"};
    }

    if (level == 2) {
        if (pos == 1) {
            return {"Engineer"};
        } else {
            return {"Doctor"};
        }
    }

    if ((pos % 2) == 1) {
        return findProfession(level - 1, (pos + 1) / 2);
    } else {
        if (findProfession(level - 1, (pos + 1) / 2) == "Doctor") {
            return {"Engineer"};
        } else {
            return {"Doctor"};
        }
    }
}

int main() {
    std::cout << findProfession(3, 3) << std::endl;
}