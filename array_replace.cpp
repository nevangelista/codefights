#include <algorithm>
#include <iostream>
#include <vector>

std::vector<int>
arrayReplace(std::vector<int> inputArray, int elemToReplace, int substitutionElem) {
    std::replace_if(
            inputArray.begin(),
            inputArray.end(),
            [=](int elm) { return elm == elemToReplace; },
            substitutionElem);

    return inputArray;
}

void printOutput(std::vector<int>& outputArray) {
    std::cout << "[";
    auto i = 0;
    for (auto mod : outputArray) {
        std::cout << mod;
        if (i < (outputArray.size() - 1)) {
            std::cout << ", ";
        }
        ++i;
    }
    std::cout << "]" << std::endl;
}

int main() {
    std::vector<int> mangle{1, 2, 1};

    auto replace = 1;
    auto subwith = 3;

    std::vector<int> modded = arrayReplace(mangle, replace, subwith);
    printOutput(modded);

    return 0;
}
