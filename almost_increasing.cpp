#include <iostream>
#include <vector>

void printseq(std::vector<int>& sequence) {
    for (auto seq : sequence) {
        std::cout << seq << " ";
    }
    std::cout << std::endl;
}

std::vector<int> checkLongestIncreasingSequence(std::size_t seq_size, std::vector<int> sequence) {
    if (sequence.empty()) {
        return sequence;
    }

    std::vector<int> longest{sequence[0]};
    auto last = sequence[0];
    for (auto i = 1; i < sequence.size(); ++i) {
        auto current = sequence[i];
        if (current - last > 0) {
            longest.push_back(current);
            last = current;
            continue;
        } else {
            std::vector<int> check1{longest};
            check1.pop_back();
            check1.insert(check1.end(), sequence.begin() + i, sequence.end());
            if ((seq_size - check1.size()) <= 1) {
                check1 = checkLongestIncreasingSequence(seq_size, check1);
            }

            std::vector<int> check2{longest};
            check2.insert(check2.end(), sequence.begin() + i + 1, sequence.end());
            if (seq_size - check2.size() <= 1) {
                check2 = checkLongestIncreasingSequence(seq_size, check2);
            }

            return (check1.size() > check2.size()) ? check1 : check2;
        }
    }

    return longest;
}

bool almostIncreasingSequence(std::vector<int> sequence) {
    if (sequence.empty()) {
        return false;
    }

    std::vector<int> longest = checkLongestIncreasingSequence(sequence.size(), sequence);
    return (sequence.size() - longest.size() <= 1);
}

int main() {
    std::vector<int> test{1, 2, 3, 4, 99, 5, 6};

    std::cout << std::boolalpha << almostIncreasingSequence(test) << std::endl;
    return 0;
}
