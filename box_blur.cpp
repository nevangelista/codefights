#include <cmath>
#include <iostream>
#include <numeric>
#include <vector>

int doBox(std::vector<int>& boxvals) {
    constexpr double boxpop = 9.0;
    double boxval = std::accumulate(boxvals.begin(), boxvals.end(), 0.0);
    double floorme = boxval / boxpop;
    return std::floor(floorme);
}

void printBlur(std::vector<std::vector<int>>& blur) {
    std::cout << "[";
    auto r = 0;
    for (auto rows : blur) {
        if (r > 0) {
            std::cout << " ";
        }
        std::cout << "[";

        auto i = 0;
        for (auto blurval : rows) {
            std::cout << blurval;
            if (i < rows.size() - 1) {
                std::cout << ",";
            }
            ++i;
        }

        std::cout << "]";
        if (r < blur.size() - 1) {
            std::cout << "," << std::endl;
        }
        ++r;
    }
    std::cout << "]" << std::endl;
}

std::vector<std::vector<int>> boxBlur(std::vector<std::vector<int>> image) {
    constexpr auto boxdim = 3;
    constexpr auto boxsize = boxdim * boxdim;
    auto idim = image.size();
    auto jdim = image[0].size();

    std::vector<std::vector<int>> boxes;
    for (auto i = 0; i < idim - boxdim + 1; ++i) {
        std::vector<int> boxvals;
        for (auto j = 0; j < jdim - boxdim + 1; ++j) {
            std::vector<int> blurmat;
            for (auto k = i; k < i + boxdim; ++k) {
                auto row = image[k];
                blurmat.insert(blurmat.end(), row.begin() + j, row.begin() + j + boxdim);
            }
            auto blur = doBox(blurmat);
            boxvals.push_back(blur);
        }

        boxes.push_back(boxvals);
    }

    printBlur(boxes);
    return boxes;
}

int main() {
    std::vector<std::vector<int>> image{{172, 14, 85, 186, 28, 155, 122, 39, 44},
                                        {71, 215, 117, 16, 56, 107, 126, 114, 214},
                                        {83, 173, 135, 218, 151, 200, 133, 58, 8},
                                        {249, 119, 57, 134, 255, 70, 82, 238, 37},
                                        {199, 76, 110, 36, 121, 185, 187, 82, 92},
                                        {106, 231, 120, 126, 41, 205, 203, 57, 185},
                                        {246, 241, 81, 146, 148, 46, 16, 128, 233}};

    std::vector<std::vector<int>> blur = boxBlur(image);

    return 0;
}
