#include <algorithm>
#include <iostream>
#include <limits>
#include <numeric>
#include <vector>

struct AbsSum {
    AbsSum(int choice) : sum(0), choice(choice) {}

    void operator()(int a) {
        sum += std::abs(a - choice);
    }

    int sum;
    int choice;
};

int absoluteValuesSumMinimization(std::vector<int> a) {
    auto minsum = std::numeric_limits<int>::max();
    auto minchoice = 0;

    for (auto it = a.begin(); it != a.end(); ++it) {
        auto choice = *it;
        std::vector<int> rest(a.begin(), it);
        rest.insert(rest.end(), it + 1, a.end());

        AbsSum cursum = std::for_each(rest.begin(), rest.end(), AbsSum(choice));
        if (cursum.sum < minsum) {
            minsum = std::min(minsum, cursum.sum);
            minchoice = choice;
        }
    }

    return minchoice;
}

int main() {
    std::vector<int> summit{-10, -10, -10, -10, -10, -9, -9, -9, -8, -8, -7, -6, -5, -4, -3,
                            -2,  -1,  0,   0,   0,   0,  1,  2,  3,  4,  5,  6,  7,  8,  9,
                            10,  11,  12,  13,  14,  15, 16, 17, 18, 19, 20, 21, 22, 23, 24,
                            25,  26,  27,  28,  29,  30, 31, 32, 33, 34, 35, 36, 37, 38, 39,
                            40,  41,  42,  43,  44,  45, 46, 47, 48, 49, 50};

    std::cout << absoluteValuesSumMinimization(summit) << std::endl;
}
