#include <array>
#include <cctype>
#include <iostream>
#include <map>
#include <vector>

std::vector<std::array<char, 8>> genChessBoard() {
    std::array<char, 8> blackrow{'b', 'w', 'b', 'w', 'b', 'w', 'b', 'w'};

    std::array<char, 8> whiterow{'w', 'b', 'w', 'b', 'w', 'b', 'w', 'b'};

    std::vector<std::array<char, 8>> board{
            blackrow, whiterow, blackrow, whiterow, blackrow, whiterow, blackrow, whiterow};

    return board;
}

bool chessBoardCellColor(std::string cell1, std::string cell2) {
    std::map<char, int> colmap{
            {'a', 0}, {'b', 1}, {'c', 2}, {'d', 3}, {'e', 4}, {'f', 5}, {'g', 6}, {'h', 7}};

    std::vector<std::array<char, 8>> board = genChessBoard();

    auto colidx = colmap.at(std::tolower(cell1[0]));
    auto rowidx = (cell1[1] - '0') - 1;
    auto color1 = board[rowidx][colidx];

    colidx = colmap.at(std::tolower(cell2[0]));
    rowidx = (cell2[1] - '0') - 1;
    auto color2 = board[rowidx][colidx];

    return (color1 == color2);
}

int main() {
    std::cout << std::boolalpha << chessBoardCellColor("A1", "C3") << std::endl;
    std::cout << std::boolalpha << chessBoardCellColor("A1", "H3") << std::endl;
}