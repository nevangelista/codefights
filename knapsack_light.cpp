#include <iomanip>
#include <iostream>
#include <vector>

int knapsackLight(int value1, int weight1, int value2, int weight2, int maxW) {
    constexpr auto numItems = 2;
    std::vector<int> values{0, value1, value2};

    std::vector<int> weights{0, weight1, weight2};

    int knap[numItems + 1][maxW + 1];

    for (auto i = 0; i <= maxW; ++i) {
        knap[0][i] = 0;
    }

    for (auto i = 1; i <= numItems; ++i) {
        for (auto j = 0; j <= maxW; ++j) {
            if (weights[i] > j) {
                knap[i][j] = knap[i - 1][j];
            } else {
                auto case1 = knap[i - 1][j];
                auto case2 = values[i] + knap[i - 1][j - weights[i]];
                knap[i][j] = std::max(case1, case2);
            }
        }
    }

    for (auto i = 0; i <= numItems; ++i) {
        for (auto j = 0; j <= maxW; ++j) {
            std::cout << std::setw(2) << knap[i][j] << " ";
        }
        std::cout << std::endl;
    }

    return knap[numItems][maxW];
}

int main() {
    std::cout << knapsackLight(10, 5, 6, 4, 8) << std::endl;
    std::cout << knapsackLight(10, 5, 6, 4, 9) << std::endl;

    return 0;
}
