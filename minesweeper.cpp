#include <iostream>
#include <vector>

int check_neighbors(int rowdim, int coldim, int loc, bool* seeds) {
    bool ledge = (loc % coldim == 0);
    bool uedge = (loc - coldim < 0);
    bool dedge = (loc + coldim > rowdim * coldim - 1);
    bool redge = ((loc + 1) % coldim == 0);

    auto ul = (ledge || uedge) ? -1 : loc - 1 - coldim;
    auto ls = ledge ? -1 : loc - 1;
    auto ll = (ledge || dedge) ? -1 : loc - 1 + coldim;

    auto up = uedge ? -1 : loc - coldim;
    auto dn = dedge ? -1 : loc + coldim;

    auto ur = (redge || uedge) ? -1 : loc + 1 - coldim;
    auto rs = redge ? -1 : loc + 1;
    auto lr = (redge || dedge) ? -1 : loc + 1 + coldim;

    auto minecount = 0;
    if (ul != -1) {
        if (seeds[ul]) {
            ++minecount;
        }
    }

    if (ls != -1) {
        if (seeds[ls]) {
            ++minecount;
        }
    }

    if (ll != -1) {
        if (seeds[ll]) {
            ++minecount;
        }
    }

    if (up != -1) {
        if (seeds[up]) {
            ++minecount;
        }
    }

    if (dn != -1) {
        if (seeds[dn]) {
            ++minecount;
        }
    }

    if (ur != -1) {
        if (seeds[ur]) {
            ++minecount;
        }
    }

    if (rs != -1) {
        if (seeds[rs]) {
            ++minecount;
        }
    }

    if (lr != -1) {
        if (seeds[lr]) {
            ++minecount;
        }
    }

    return minecount;
}

void mark_mines(int rowdim, int coldim, bool* seeds, int* mines) {
    for (auto loc = 0; loc < rowdim * coldim; ++loc) {
        auto count = check_neighbors(rowdim, coldim, loc, seeds);
        mines[loc] = count;
    }
}

void print_minefield(std::vector<std::vector<int>>& mines) {
    auto i = 0;
    std::cout << "[";
    for (auto row : mines) {
        auto j = 0;
        if (i > 0) {
            std::cout << " ";
        }
        std::cout << "[";
        for (auto col : row) {
            std::cout << col;
            if (j < row.size() - 1) {
                std::cout << ",";
            }
            ++j;
        }
        std::cout << "]";
        if (i < mines.size() - 1) {
            std::cout << "," << std::endl;
        }
        ++i;
    }
    std::cout << "]" << std::endl;
}

std::vector<std::vector<int>> minesweeper(std::vector<std::vector<bool>> matrix) {
    auto rowdim = matrix.size();
    auto coldim = matrix[0].size();

    bool seeds[rowdim * coldim];
    auto i = 0;
    for (auto row : matrix) {
        for (auto col : row) {
            seeds[i] = col;
            ++i;
        }
    }

    int mines[rowdim * coldim];
    mark_mines(rowdim, coldim, seeds, mines);

    std::vector<std::vector<int>> marked;
    auto loc = 0;
    for (auto i = 0; i < rowdim; ++i) {
        std::vector<int> row;
        for (auto j = 0; j < coldim; ++j) {
            row.push_back(mines[loc]);
            ++loc;
        };
        marked.push_back(row);
    }

    print_minefield(marked);
    return marked;
}

int main() {
    std::vector<std::vector<bool>> seeds{
            {true, false, false, true}, {false, false, true, false}, {true, true, false, true}};

    std::vector<std::vector<int>> mines = minesweeper(seeds);

    return 0;
}